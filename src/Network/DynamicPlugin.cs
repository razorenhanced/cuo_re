﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Reflection.Emit;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using ClassicUO.Configuration;
using ClassicUO.Game;
using ClassicUO.Game.Data;
using ClassicUO.Game.Managers;
using ClassicUO.Game.UI.Controls;
using ClassicUO.Game.UI.Gumps;
using ClassicUO.IO.Resources;
using ClassicUO.Renderer;
using ClassicUO.Renderer.Batching;
using ClassicUO.Utility.Logging;
using ClassicUO.Utility.Platforms;
using CUO_API;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using SDL2;
using System.Linq;



//Dalamar: +1 file

namespace ClassicUO.Network
{
    internal unsafe class DynamicPlugin
    {

        //Delegates
        [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
        private delegate void OnInstall(void* header);

        //Function Pointers
        [MarshalAs(UnmanagedType.FunctionPtr)] private OnInitialize _onInitialize;
        [MarshalAs(UnmanagedType.FunctionPtr)] private OnTick _tick;


        public readonly int HeaderVersion = 0;




        private DynamicPlugin(string path)
        {
            PluginPath = path;
        }

        public static List<DynamicPlugin> Plugins { get; } = new List<DynamicPlugin>();

        public string PluginPath { get; }

        public bool IsValid { get; private set; }




        public static DynamicPlugin Create(string path)
        {
            path = Path.GetFullPath(Path.Combine(CUOEnviroment.ExecutablePath, "Data", "Plugins", path));

            if (!File.Exists(path))
            {
                Log.Error($"Plugin '{path}' not found.");

                return null;
            }

            Log.Trace($"Loading plugin: {path}");

            DynamicPlugin p = new DynamicPlugin(path);
            p.Load();

            if (!p.IsValid)
            {
                Log.Warn($"Invalid plugin: {path}");

                return null;
            }

            Log.Trace($"Plugin: {path} loaded.");
            Plugins.Add(p);

            return p;
        }




        public struct DynamicPluginHeader
        {
            public int HeaderVersion;
            public int ClientVersion;

            public IntPtr OnInitialize;
            public IntPtr Tick;
        }

        public void Load()
        {
            DynamicOptions.Setup(); 


            DynamicPluginHeader header = new DynamicPluginHeader
            {
                HeaderVersion = HeaderVersion,
                ClientVersion = (int)Client.Version,
            };

            //TODO: check if structs are passed by reference 
            if (PluginHandshake(PluginPath, header)) {
                IsValid = BindMethods(header);

                _onInitialize?.Invoke();
            }










        }


        public bool PluginHandshake(string PluginPath, DynamicPluginHeader header)
        {

            // Handshake between CUO and it's plugins

            SDL.SDL_SysWMinfo info = new SDL.SDL_SysWMinfo();
            SDL.SDL_VERSION(out info.version);
            SDL.SDL_GetWindowWMInfo(Client.Game.Window.Handle, ref info);

            IntPtr hwnd = IntPtr.Zero;

            if (info.subsystem == SDL.SDL_SYSWM_TYPE.SDL_SYSWM_WINDOWS)
            {
                hwnd = info.info.win.window;
            }

            void* func = &header;


            try
            {
                IntPtr assptr = Native.LoadLibrary(PluginPath);

                Log.Trace($"assembly: {assptr}");

                if (assptr == IntPtr.Zero)
                {
                    throw new Exception("Invalid Assembly, Attempting managed load.");
                }

                Log.Trace($"Searching for 'Install' entry point  -  {assptr}");

                IntPtr installPtr = Native.GetProcessAddress(assptr, "Install");

                Log.Trace($"Entry point: {installPtr}");

                if (installPtr == IntPtr.Zero)
                {
                    throw new Exception("Invalid Entry Point, Attempting managed load.");
                }

                Marshal.GetDelegateForFunctionPointer<OnInstall>(installPtr)(func);

                Console.WriteLine(">>> ADDRESS {0}", header.OnInitialize);
            }
            catch
            {
                try
                {
                    Assembly asm = Assembly.LoadFile(PluginPath);
                    Type type = asm.GetType("Assistant.Engine");

                    if (type == null)
                    {
                        Log.Error("Unable to find Plugin Type, API requires the public class Engine in namespace Assistant.");

                        return false;
                    }

                    MethodInfo meth = type.GetMethod("InstallDynamic", BindingFlags.Public | BindingFlags.Static);

                    if (meth == null)
                    {
                        Log.Error("Engine class missing public static InstallDynamic method Needs 'public static unsafe void InstallDynamic(DynamicPluginHeader *plugin)' ");

                        return false;
                    }


                    meth.Invoke(null, new object[] { (IntPtr)func });
                }
                catch (Exception err)
                {
                    Log.Error($"Plugin threw an error during Initialization. {err.Message} {err.StackTrace} {err.InnerException?.Message} {err.InnerException?.StackTrace}");

                    return false;
                }
            }

            return true;
        }


        public bool BindMethods(DynamicPluginHeader header) {
            if (header.Tick != IntPtr.Zero)
            {
                _tick = Marshal.GetDelegateForFunctionPointer<OnTick>(header.Tick);
            }
            if (header.OnInitialize != IntPtr.Zero)
            {
                _onInitialize = Marshal.GetDelegateForFunctionPointer<OnInitialize>(header.OnInitialize);
            }


            return true;
        }



    }


    class DynamicOptions{
        static byte FONT = 0xFF;
        static ushort HUE_FONT = 0xFFFF;
        static int WIDTH = 700;
        static int HEIGHT = 500;
        static int TEXTBOX_HEIGHT = 25;
        static int PAGE = 13;

        
        public static Dictionary<string, Control> controls = new Dictionary<string, Control>();
        public static Dictionary<string, ScrollArea> sections = new Dictionary<string, ScrollArea>();


        public static void Setup() {
            Observable.AddMethodObserver(typeof(GameActions), "OpenSettings", typeof(DynamicOptions), "OnOpen");
        }

        public static void OnOpen()
        {
            var custom_settings = Build_Settings();
            AddSection("CUO.RE", custom_settings);
            var options_gump = GetOptionsGump();
            Observable.AddMethodObserver(options_gump.GetType(), "Apply", typeof(DynamicOptions), "OnSave");
            
            GameActions.OpenSettings();
        }

        public static void OnSave() {
            var isEnabled = GetControlValue("CUORE.Enable");
        }
        
        public static string GetControlValue(string name) {
            if (!controls.ContainsKey(name)) { return null; }
            Control control = controls[name];
            if (control.GetType() == typeof(Checkbox)) {
                return ((Checkbox)control).IsChecked.ToString();
            }

            return name;
        }


        public static OptionsGump GetOptionsGump() {
            // Return Existing
            OptionsGump options_gump = UIManager.GetGump<OptionsGump>();
            if (options_gump != null) { return options_gump; }

            // Create New and cache
            options_gump = new OptionsGump();
            options_gump.X = (Client.Game.Window.ClientBounds.Width >> 1) - 300;
            options_gump.Y = (Client.Game.Window.ClientBounds.Height >> 1) - 250;
            UIManager.Add(options_gump);
            return options_gump;
        }


        


        public static void AddSection(string name, ScrollArea option_page)
        {
            var options_gump = GetOptionsGump();


            var nice_buttons = options_gump.GetControls<NiceButton>(); // too many
            var sections_buttons = nice_buttons.Select(btn => btn.ButtonParameter > 0 && btn.Parent == options_gump );
            var section_cnt = sections_buttons.Count() - 2; // -2 -> it's for "Add Macro"/"Remove Macro" they are not inside the macro panel 
            var page = section_cnt;

            options_gump.Add
            (
                new NiceButton
                (
                    10,
                    10 + 30 * page,
                    140,                                                  
                    25,
                    ButtonAction.SwitchPage,
                    name
                )
                { ButtonParameter = page+1 }
            );
            options_gump.Add(option_page, page+1);

            sections[name] = option_page;
        }


        public static ScrollArea EmptyOptionPage()
        {
            ScrollArea rightArea = new ScrollArea
            (
                190,
                20,
                WIDTH - 210,
                420,
                true
            );


            return rightArea;
        }

        public static ScrollArea Build_Settings() {

            ScrollArea option_page = EmptyOptionPage();


            int startX = 5;
            int startY = 5;


            controls["CUORE.Enable"] = new Checkbox(0x00D2, 0x00D3, "Enable Options integration", FONT, HUE_FONT)
            {
                IsChecked = true,
                X = startX,
                Y = startY
            };
            startY += controls["CUORE.Enable"].Height + 2;


            controls["CUORE.Slider1"] = new Checkbox(0x00D2, 0x00D3, "Enable Options integration", FONT, HUE_FONT)
            {
                IsChecked = true,
                X = startX,
                Y = startY
            };
            startX += controls["CUORE.Slider1"].Height + 2;

            foreach (var name in controls.Keys) {
                var control = controls[name];
                option_page.Add(control);
            }
            
            return option_page;

        }
        
    }





    // Useful code by a very smart guy, for reference:
    // https://github.com/spinico/MethodRedirect
    // https://stackoverflow.com/questions/7299097/dynamically-replace-the-contents-of-a-c-sharp-method/7299396#7299396
    // 
    // Dalamar: Adaped to provide a method Wrapper.
    // TODO 1: add static caching by Type.FullName (qualified), this will allow for next todo:
    // TODO 2: add method that return the static "observer" which can add/remove callbacks for onBefore, onAfter

    public static class Observable
    {

        public static bool AddMethodObserver(Type observed, string method_observed, Type observer, string method_observer)
        {
            try
            {
                BindingFlags flags = BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Static | BindingFlags.Instance | BindingFlags.DeclaredOnly;
                MethodInfo observed_method = observed.GetMethod(method_observed, flags );
                MethodInfo observer_method = observer.GetMethod(method_observer, flags );
                observer_method.Observe(observed_method);
            }
            catch
            {
                return false;
            }
            return true;
        }

        public static void ObservedBy(this MethodInfo origin, MethodInfo target)
        {

            IntPtr ori = GetMethodAddress(origin);
            IntPtr tar = GetMethodAddress(target);

            Marshal.Copy(new IntPtr[] { Marshal.ReadIntPtr(tar) }, 0, ori, 1);
        }

        public static void Observe(this MethodInfo origin, MethodInfo target)
        {
            target.ObservedBy(origin);
        }

        private static IntPtr GetMethodAddress(MethodInfo mi)
        {
            const ushort SLOT_NUMBER_MASK = 0xfff; // 3 bytes
            const int MT_OFFSET_32BIT = 0x28;      // 40 bytes
            const int MT_OFFSET_64BIT = 0x40;      // 64 bytes

            IntPtr address;

            // JIT compilation of the method
            RuntimeHelpers.PrepareMethod(mi.MethodHandle);

            IntPtr md = mi.MethodHandle.Value;             // MethodDescriptor address
            IntPtr mt = mi.DeclaringType.TypeHandle.Value; // MethodTable address

            if (mi.IsVirtual)
            {
                // The fixed-size portion of the MethodTable structure depends on the process type
                int offset = IntPtr.Size == 4 ? MT_OFFSET_32BIT : MT_OFFSET_64BIT;

                // First method slot = MethodTable address + fixed-size offset
                // This is the address of the first method of any type (i.e. ToString)
                IntPtr ms = Marshal.ReadIntPtr(mt + offset);

                // Get the slot number of the virtual method entry from the MethodDesc data structure
                // Remark: the slot number is represented on 3 bytes
                long shift = Marshal.ReadInt64(md) >> 32;
                int slot = (int)(shift & SLOT_NUMBER_MASK);

                // Get the virtual method address relative to the first method slot
                address = ms + (slot * IntPtr.Size);
            }
            else
            {
                // Bypass default MethodDescriptor padding (8 bytes) 
                // Reach the CodeOrIL field which contains the address of the JIT-compiled code
                address = md + 8;
            }

            return address;
        }
    }
}